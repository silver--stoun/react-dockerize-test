#!/usr/bin/make

DOCKER_CONTAINER_NAME ?= docker-react-container
DOCKER_IMAGE_NAME ?= docker-react-image
DOCKER_IMAGE_VERSION ?= 1.0

.PHONY : help up down

.DEFAULT_GOAL := help

help:
	@echo "\n \
	*********************\n \
	*** Make commands ***\n \
	*********************\n\n \
	help	- Show this help\n \
	up	- Start backend containers (you can send param CONTAINER)\n \
	down	- Stop backend containers\n \
	build	- build projects image\n \
	rebuild	- Rebuild project \
	"

up: build
	@docker run -d -p 4000:80 --name ${DOCKER_CONTAINER_NAME} ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}

down:
	@docker stop ${DOCKER_CONTAINER_NAME}
	@docker rm ${DOCKER_CONTAINER_NAME}

clear: down
	@docker rmi ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}

build:
	@docker build -t docker-react-image:1.0 .

rebuild: clear up
